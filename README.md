# Code analysis tools, profilers, debuggers
* [Very Sleepy](http://www.codersnotes.com/sleepy/) - free C/C++ CPU profiler for Windows systems
* [Valgrind](http://valgrind.org/) - framework for building dynamic analysis tools
* [Sanitizers](https://github.com/google/sanitizers) - AddressSanitizer, MemorySanitizer, ThreadSanitizer, LeakSanitizer by Google
* [gdbgui](https://github.com/cs01/gdbgui) - Browser-based frontend to gdb (gnu debugger). Add breakpoints, view the stack, visualize data structures, and more in C, C++, Go, Rust, and Fortran. Run gdbgui from the terminal and a new tab will open in your browser.

# Tools
* [C++ shell](http://cpp.sh/) - C++ online compiler

# Libraries
* [Signal Processing](http://eceweb1.rutgers.edu/~orfanidi/intro2sp/#cfunct)
* [QuantLib](https://www.quantlib.org/) - A free/open-source library for quantitative finance
* [POCO](https://pocoproject.org/) - cross-platform C++ libraries for building network and internet-based applications
* [iprof](https://github.com/Neurochrom/iprof) - A pretty simple and performant C++ profiling library
* [simple-socket](https://github.com/bernhardHartleb/simple-socket) - An easy to use C++ socket and network library, mainly for UNIX systems
* [scnlib](https://github.com/eliaskosunen/scnlib) - scanf for modern C++
* [simdjson](https://github.com/lemire/simdjson) - Parsing gigabytes of JSON per second
* [numeris romanis](https://github.com/tcbrindle/numeris_romanis) - Roman numeral support for C++17

### Web development related
* [Crow](https://github.com/ipkn/crow) - Crow is C++ microframework for web. (inspired by Python Flask)
* [Mongoose](https://github.com/cesanta/mongoose) - Mongoose Embedded Web Server Library - Mongoose is more than an embedded webserver. It is a multi-protocol embedded networking library with functions including TCP, HTTP client and server, WebSocket client and server, MQTT client and broker and much more
* [Silicon](http://siliconframework.org/) - Silicon is a C++ abstraction built on top of high-performance networking libraries. Its goal is to ease the writing of web APIs without compromising on performance
* [Proxygen: Facebook's C++ HTTP Libraries](https://github.com/facebook/proxygen) - A collection of C++ HTTP libraries including an easy to use HTTP server
* [nghttp2](https://github.com/nghttp2/nghttp2) - an implementation of the Hypertext Transfer Protocol version 2 in C
* [Emscripten](https://github.com/emscripten-core/emscripten) - an LLVM-to-Web Compiler

### Graphics
* [SFML - Simple and Fast Multimedia Library](https://www.sfml-dev.org/)

### GUI
* [C++ UI Libraries list (April 06 2019)](https://philippegroarke.com/posts/2018/c++_ui_solutions/)

### Game Engines
* [FIFE](http://www.fifengine.net/index.html)
 
### Other
* [GSL: Guidelines Support Library](https://github.com/Microsoft/GSL) / [modernescpp](http://www.modernescpp.com/index.php/c-core-guideline-the-guidelines-support-library)
* [Fruit](https://github.com/google/fruit)  - dependency injection framework
* [Vinbero](https://github.com/vinbero/vinbero) - modular server
* [buckaroo](https://github.com/LoopPerfect/buckaroo) - The decentralized package manager for C++ and friends
 
# Videos
* [CppCon 2017: Kate Gregory “10 Core Guidelines You Need to Start Using Now](https://www.youtube.com/watch?v=XkDEzfpdcSg)
* [C++Now 2019: Michael Park “Pattern Matching: Match Me If You Can”](https://www.youtube.com/watch?v=nOwUzFYt0NQ)

# Topics/Explanations/Tutorials
* [The Exceptional Beauty of Doom 3's Source Code](http://kotaku.com/5975610/the-exceptional-beauty-of-doom-3s-source-code)
* [Implementing State Machines with std::variant](http://khuttun.github.io/2017/02/04/implementing-state-machines-with-std-variant.html)
* [C++ Core Guidelines](https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md)
* [POINTERS - (PL) shared_ptr/unique_ptr](https://kacperkolodziej.pl/programowanie/263-cpp11-wskazniki-unikalne-i-wspoldzielone-unique-ptr-shared-ptr.html)
* [POINTERS - make_shared vs. normal shared_ptr](https://stackoverflow.com/questions/20895648/difference-in-make-shared-and-normal-shared-ptr-in-c)
* [POINTERS - Exploring std::shared_ptr](https://shaharmike.com/cpp/shared-ptr/)
* [POINTERS - Exploring std::unique_ptr](https://shaharmike.com/cpp/unique-ptr/)
* [POINTERS - Using C++11’s Smart Pointers (pdf)](http://umich.edu/~eecs381/handouts/C++11_smart_ptrs.pdf)
* [POINTERS - Performance of raw pointers vs smart pointers in C++11](http://blog.davidecoppola.com/2016/10/performance-of-raw-pointers-vs-smart-pointers-in-cpp/)
* [LAMBDAS - From C++ to C++20](https://www.bfilipek.com/2019/02/lambdas-story-part1.html)
* [LAMBDAS - Under the hood of lambdas and std::function](https://shaharmike.com/cpp/lambdas-and-functions/)
* [SOLID - Bob Martin SOLID Principles of Object Oriented and Agile Design](https://www.youtube.com/watch?v=TMuno5RZNeE&t=3120s)
* [SOLID - #5 - Dependency Injection (Kris Jusiak @ cppnow)](https://www.youtube.com/watch?time_continue=0&v=yVogS4NbL6U)
* [SOLID - #5 - Boost.DI - Inject all the things!](https://www.youtube.com/watch?v=8HmjM3G8jhQ)
* [DESIGN PATTERNS - Badge](https://awesomekling.github.io/Serenity-C++-patterns-The-Badge/)
* [KEYWORDS - auto - C++ auto and decltype Explained](http://thbecker.net/articles/auto_and_decltype/section_01.html)
* [MULTITHREADING - C++11 multithreading](https://solarianprogrammer.com/2011/12/16/cpp-11-thread-tutorial/)
* [MULTITHREADING - C++14 multithreading](https://hackernoon.com/learn-c-multi-threading-in-5-minutes-8b881c92941f)
* [SERIALIZATION - Serialization and Unserialization IsoCpp](https://isocpp.org/wiki/faq/serialization)
* [SERIALIZATION - A practical guide to C++ serialization](http://www.ocoudert.com/blog/2011/07/09/a-practical-guide-to-c-serialization/)
* [SERIALIZATION - Ultra-fast Serialization of C++ Objects](https://accu.org/index.php/journals/2317)
* [VAR - Understanding the meaning of lvalues and rvalues in C++](https://www.internalpointers.com/post/understanding-meaning-lvalues-and-rvalues-c)
* [VAR - Virtual Method Table Hooking Explained](https://niemand.com.ar/2019/01/30/virtual-method-table-hooking-explained/?explained)
* [VAR - Dark Corner of C++ Corner Cases](https://www.bfilipek.com/2019/03/darker-cpp.html)
* [VAR - Optimizing C++ by Avoiding Moves](https://www.jeremyong.com/c++17/metaprogramming/2019/03/12/optimizing-cpp-by-avoiding-moves.html)
* [VAR - C++ Tutorial: Auto Registering Factory](http://derydoca.com/2019/03/c-tutorial-auto-registering-factory/)
* [VAR - Some awesome modern C++ features that every developer should know](https://medium.freecodecamp.org/some-awesome-modern-c-features-that-every-developer-should-know-5e3bf6f79a3c)
* [VAR - Initialization in C++ is Seriously Bonkers](http://mikelui.io/2019/01/03/seriously-bonkers.html)

### Articles
* [Cheerp 2.0 released — the most powerful Cheerp yet](https://medium.com/leaningtech/cheerp-2-0-release-880f249a5677) - C/C++ to WebAssembly/JavaScript compiler
* [Generic Interfaces with Generic Lambdas with C++ and SYCL](https://www.codeplay.com/portal/06-11-19-sycl-cpp-generic-interfaces-with-generic-lambdas)
 
### Design Patterns
* [Vince Huston's Design Patterns](http://www.vincehuston.org/dp/)
* [Source Making's Design Patterns](https://sourcemaking.com/design_patterns)
* [c++ patterns](https://cpppatterns.com/)

# Various
* [Doom 3 Source Code Review](http://fabiensanglard.net/doom3/index.php)
* [How do I generate true random binary numbers in C++?](https://www.quora.com/How-do-I-generate-true-random-binary-numbers-in-C++)
* [Efficiently Generating a Number in a Range](http://www.pcg-random.org/posts/bounded-rands.html)
* [Circle - a new programming language that extends C++ 17](https://www.circle-lang.org/)

# Websites/blogs
* [ISO Cpp](https://isocpp.org/)
* [ACCU](https://accu.org/index.php) / [Journals](https://accu.org/index.php/journals/)
* [GER/Modern C++](https://www.grimm-jaud.de/index.php/blog)
* [Andreas Fertig blog](https://www.andreasfertig.blog/)
* [Bartłomiej Filipek blog](https://www.bfilipek.com/)
 
# Tutorial/Courses
* [Jason Turner Youtube tutorials](https://www.youtube.com/user/lefticus1)

# C Language
* [So you think you know C?](https://wordsandbuttons.online/so_you_think_you_know_c.html)